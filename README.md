
# Step to build current repo

```shell

cd /path/to/local/workspace
git clone -b develop git@bitbucket.org:liuruibnu/tib_bw_ci_poc.git

cd /tibco_home/tibco/bwce/bwce/2.3/bin
./bwdesign -data ~/Downloads/tempworkspace system:import -f /Users/ruiliu/Downloads/temp/workspacetest/tib_bw_ci_poc
./bwdesign -data ~/Downloads/tempworkspace system:export hello.world.application ~/Downloads

```

# Ways to override params



# Integrate with Slack

Tools installed and configured, test noticifation 
Edit notification settings (add subscription for Develop Branch)

# TIB_BW_CI_POC

Tibco BusinessWorks CI CD process POC


## Steps

* Step 1, Develop using Eclipse IDE , do unit test


Set up environment

```shell
TIB_bwce-dev_2.3.4_macosx_x86_64 ruiliu$ ./TIBCOUniversalInstaller-mac.command







          Initializing Wizard........
          Launching InstallShield Wizard.......

```
Run IDE

```shell
./TIBCOBusinessStudio
```

Issue with SVN
https://docs.tibco.com/pub/activematrix_businessworks/6.2.2/doc/html/GUID-E8F49AD8-8312-497C-9034-2882B54A322D.html

* Step 2, Code & Test data (Unit/CIT) & environment definition file commit to git repo.
  * Option 1, commit to bitbucket
  * Option 2, commit to Github

* Step 3,  Commit action trigger pipeline

  * Option 1, use EC2 with jenkin (or local jenkin) to pull the code and build locally, get deployable ear and push to S3

  * Option 2, use bitbucket pipeline to build remotely to get deployable  ear and push ear to amazon S3 --- might not work; as the build environment is too heavy to exist in bitbuckt pipeline evironment (???)
  * Option 3,  after get deployable ear and git version control the ear and let the aws container using pull to get the deployable ear

* Step 4,  Continue: deploy ear to DEV container (use the definition file) and trigger the automated test (using the committed test data)

```readme
Scripts for customizing Docker images for TIBCO BusinessWorks™ Container Edition
================================================================================================
Use these scripts to generate a TIBCO BusinessWorks™ Container Edition (BWCE)
2.x Docker Image using the BWCE runtime
(available as bwce-runtime-<version>.zip from edelivery.tibco.com, e.g. bwce-runtime-2.3.1.zip)

Prerequisite
================================================================================================
  * Access to https://edelivery.tibco.com.
  * Docker Installation (https://docs.docker.com/engine/installation/).

Download TIBCO BusinessWorks™ Container Edition Runtime
================================================================================================
Download TIBCO BusinessWorks™ Container Edition 2.x.x runtime file (bwce-runtime-<version>.zip)
from https://edelivery.tibco.com.

Create TIBCO BusinessWorks™ Container Edition Base Docker Image
================================================================================================
  * Copy bwce-runtime-<version>.zip file to <BWCE-HOME>/docker/resources/bwce-runtime folder
  * On terminal, goto <BWCE-HOME>/docker and RUN: 'docker build -t <TAG-NAME> .'
  e.g. docker build -t tibco/bwce:latest .

Test TIBCO BusinessWorks™ Container Edition Base Docker Image
================================================================================================
  * Refer Sample documentation for Docker
```

* Step 5,  Save the test result to test repo (can be ALM/NoSQLDB/S3) and sending notification to Slack

* Step 6,  Tag the passed code in git
* Step 7,   ....


# Reference

https://github.com/aws-quickstart/quickstart-tibco-bwce
